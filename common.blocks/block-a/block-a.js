/**
 * @module block-a
 */

modules.define(
  'block-a',
  [ 'i-bem-dom' ],
  function(provide, bemDom) {

provide(bemDom.declBlock(this.name, {
  onSetMod : {
    'js' : {
      'inited' : function() {

      }
    }
  },

  methodBlockA: function() {
    
  }
}));

});