/**
 * @module block-a__elem
 */

modules.define(
  'block-a__elem',
  [ 'i-bem-dom' ],
  function(provide, bemDom) {

provide(bemDom.declElem('block-a', 'elem', {
  onSetMod : {
    'js' : {
      'inited' : function() {

      }
    }
  },

  methodBlockAElem: function() {
    
  }
}));

});