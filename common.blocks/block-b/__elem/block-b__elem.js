/**
 * @module block-b__elem
 */

modules.define(
  'block-b__elem',
  [ 'i-bem-dom', 'block-a', 'jquery' ],
  function(provide, bemDom, BlockA, $) {

provide(bemDom.declElem('block-b', 'elem', BlockA, {
  onSetMod : {
    'js' : {
      'inited' : function() {
        this._domEvents()
          .on('click', this.methodBlockBElem);
      }
    }
  },

  methodBlockBElem: function(e) {
    e.stopPropagation();
    this.setMod('clicked', true);
    console.log('`block-b__elem` hasClass `block-b__elem_js_inited`', $(this.domElem).hasClass('block-b__elem_js_inited'));
    console.log('`block-b__elem` hasClass `elem_js_inited`', $(this.domElem).hasClass('elem_js_inited'));
  }
}));

});