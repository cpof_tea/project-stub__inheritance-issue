/**
 * @module block-b
 */

modules.define(
  'block-b',
  [ 'i-bem-dom', 'block-a__elem', 'jquery' ],
  function(provide, bemDom, BlockAElem, $) {

provide(bemDom.declBlock(this.name, BlockAElem, {
  onSetMod : {
    'js' : {
      'inited' : function() {
        this._domEvents()
          .on('click', this.methodBlockB);
      }
    }
  },

  methodBlockB: function() {
    this.setMod('clicked', true);
    console.log('`block-b` hasClass `block-b_js_inited`', $(this.domElem).hasClass('block-b_js_inited'));
    console.log('`block-b` hasClass `block-b__block-b_js_inited`', $(this.domElem).hasClass('block-b__block-b_js_inited'));
  }
}));

});